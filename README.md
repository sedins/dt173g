# DT173G
___

## Syfte

Målet med denna rapport har varit att ta reda på information och besvara frågorna som ställts avseende versionshantering.

För att svara på frågorna har hjälp tagits av läroboken, föreläsningar, läsanvisningar samt sökning av information på internet. Jag har valt att uteslutande hämtat information från officiella och pålitliga källor för att få så relevanta och trovärdiga svar som möjligt.

Undersökningen gick genomgående bra då väldigt bra underlag fanns tillgängligt.

## Klona

Använd följande kommando i ett kommandofönster för att klona detta projekt

`git clone https://sedins@bitbucket.org/sedins/dt173g.git`
___

*Stefan Hälldahl, 2018-09-05*